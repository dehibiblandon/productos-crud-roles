<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Administrador de ventas</title>
<!--Data tables-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  @yield('style')
    <link rel="manifest" href="{{ asset('assets/dashboard/assets/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->

    <link href="{{ asset('assets/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/dashboard/css/datatables.min.css') }}" rel="stylesheet">

    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    
    
</head>

<body class="c-app">
  <!-- este es el panel lateral izquierdo-->
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
      <div class="c-sidebar-brand d-lg-down-none">
        <a href="#">
        <svg  class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo" style="text-decoration:none">
          <use xlink:href="{{ asset('assets/dashboard/assets/brand/coreui.svg#full')}}"> </use>
        </svg>
        </a>
      </div>

      <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
          <a class="c-sidebar-nav-link" href="{{  url('/home')  }}">
            <i class="fas fa-home"></i> Inicio  </a></li>
      
        <li class="c-sidebar-nav-title">MENÚ</li>
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{ asset('assets/dashboard/vendors/@coreui/icons/svg/free.svg#cil-puzzle') }}"></use>
            </svg> Categorias</a>
          <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/categorias"><span class="c-sidebar-nav-icon"></span> Ver listado</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/categorias/create"><span class="c-sidebar-nav-icon"></span> Crear</a></li>
          </ul>
        </li>        
  
        <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{ asset('assets/dashboard/vendors/@coreui/icons/svg/free.svg#cil-star') }}"></use>
            </svg> Productos</a>
          <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/productos"> Listado de productos</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/productos/create"> Crear producto</a></li>

          </ul>
        </li>
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{ asset('assets/dashboard/vendors/@coreui/icons/svg/free.svg#cil-bell') }}"></use>
            </svg> Ventas</a>
          <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="notifications/alerts.html"><span class="c-sidebar-nav-icon"></span> Ver listado</a></li>

          </ul>
        </li>       
      </ul>
      <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    </div>

    <div class="c-wrapper c-fixed-components">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <!-- Botón collapside-->    
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
          <svg class="c-icon c-icon-lg">
            <use xlink:href="{{ asset('assets/dashboard/vendors/@coreui/icons/svg/free.svg#cil-menu') }}"></use>
          </svg>
        </button><a class="c-header-brand d-lg-none" href="#">
          <svg width="118" height="46" alt="CoreUI Logo">
            <use xlink:href="{{ asset('assets/dashboard/assets/brand/coreui.svg#full') }}"></use>
          </svg></a>
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none"
         type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
          <svg class="c-icon c-icon-lg">
            <use xlink:href="{{ asset('assets/dashboard/vendors/@coreui/icons/svg/free.svg#cil-menu') }}"></use>
          </svg>
        </button>

        <ul class="c-header-nav d-md-down-none">
          <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="/categorias">Categorias</a></li>
          <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="/productos">Productos</a></li>
          <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="/ventas">Ventas</a></li>
        </ul>
        <ul class="c-header-nav ml-auto mr-4">
              <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
     
        </ul>   
  
      </header>
        <main class="c-main">
          <div class="container">  
            @include('flash-message')
            @yield('content')
  
          </div>
        </main>
    </div><!--c-wrapper c-fixed-components-->
    <script src="{{ asset('assets/dashboard/js/jquery-3.6.0.min.js') }}"></script>

    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('assets/dashboard/vendors/@coreui/coreui/js/coreui.bundle.min.js') }}"></script>
    <!--[if IE]><!-->
    <script src="{{ asset('assets/dashboard/vendors/@coreui/icons/js/svgxuse.min.js')}}"></script>
    <script src="{{ asset('assets/dashboard/js/datatables.min.js')}}"></script>



   
    <!--Se ubica el script de  -->
  @yield('script')

  </body>

</html>
