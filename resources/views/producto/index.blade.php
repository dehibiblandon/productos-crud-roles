@extends('layouts.app')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-nombre_producto">
                        {{ __('Listado de productos') }}
                        <button style="float: right; font-weight: 900;" class="btn btn-info btn-sm" type="button"  data-toggle="modal" data-target="#CreateProductoModal">
                            Crear Producto
                        </button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered datatable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre producto</th>
                                    <th>Cantidad</th>
                                    <th>Valor</th>
                                    <th>Estado</th>
                                    <th>Categoria</th>
                                    <th width="150" class="text-center">Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Create Producto Modal -->
<div class="modal" id="CreateProductoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-nombre_producto">Nuevo Producto</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Exitoso!</strong> añadido satisfactoriamente.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <label for="nombre_producto">Nombre producto:</label>
                    <input type="text" class="form-control" name="nombre_producto" id="nombre_producto">
                </div>
                <div class="form-group">
                    <label for="cantidad_producto">Inventario</label>
                    <input type="text" class="form-control" name="cantidad_producto" id="cantidad_producto">                    
                </div>
                <div class="form-group">
                    <label for="precio_producto_unidad">Precio unidad:</label>
                    <input type="text" class="form-control" name="precio_producto_unidad" id="precio_producto_unidad">
                </div>
                <div class="form-group">
                    <label for="categoria_id">Estado</label>
                    <input type="text" class="form-control" name="categoria_id" id="categoria_id">
                </div>
                <div class="form-group">
                    <label for="estado_id">Categoria</label>
                    <input type="text" class="form-control" name="estado_id" id="estado_id">
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateProductoForm">Crear</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Producto Modal -->
<div class="modal" id="EditProductoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-nombre_producto">Editar producto</h4>
                <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                <strong>Exitoso</strong>Producto actualizado satisfactoriamente.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="EditProductoModalBody">
                    
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitEditProductoForm">Actualizar</button>
                <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Producto Modal -->
<div class="modal" id="DeleteProductoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-nombre_producto">Eliminar Producto </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h4>Esta seguro de eliminar el producto? ésta acción no se puede deshacer</h4>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="SubmitDeleteProductoForm">Si</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 10,
            // scrollX: true,
            "order": [[ 0, "asc" ]],
            ajax: '{{ route('get-productos') }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'nombre_producto', name: 'nombre_producto'},
                {data: 'cantidad_producto', name: 'cantidad_producto'},
                {data: 'precio_producto_unidad', name: 'precio_producto_unidad'},
                {data: 'categoria_id', name: 'categoria_id'},
                {data: 'estado_id', name: 'estado_id'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });

        // Create Producto Ajax request.
        $('#SubmitCreateProductoForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('productos.store') }}",
                method: 'post',
                data: {
                    nombre_producto: $('#nombre_producto').val(),
                    cantidad_producto: $('#cantidad_producto').val(),
                    precio_producto_unidad: $('#precio_producto_unidad').val(),
                    categoria_id: $('#categoria_id').val(),
                    estado_id: $('#estado_id').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){ 
                            $('.alert-success').hide();
                            $('#CreateProductoModal').modal('hide');
                            location.reload();
                        }, 2000);
                    }
                }
            });
        });

        // Get single Producto in EditModel
        $('.modelClose').on('click', function(){
            $('#EditProductoModal').hide();
        });
        var id;
        $('body').on('click', '#getEditProductoData', function(e) {
            // e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: "productos/"+id+"/edit",
                method: 'GET',
                // data: {
                //     id: id,
                // },
                success: function(result) {
                    console.log(result);
                    $('#EditProductoModalBody').html(result.html);
                    $('#EditProductoModal').show();
                }
            });
        });

        // Update Producto Ajax request.
        $('#SubmitEditProductoForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "productos/"+id,
                method: 'PUT',
                data: {
                    nombre_producto: $('#editnombre_producto').val(),
                    cantidad_producto: $('#editcantidad_producto').val(),
                    precio_producto_unidad: $('#editprecio_producto_unidad').val(),
                    categoria_id: $('#editcategoria_id').val(),
                    estado_id: $('#editestado_id').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){ 
                            $('.alert-success').hide();
                            $('#EditProductoModal').hide();
                        }, 2000);
                    }
                }
            });
        });

        // Delete Producto Ajax request.
        var deleteID;
        $('body').on('click', '#getDeleteId', function(){
            deleteID = $(this).data('id');
        })
        $('#SubmitDeleteProductoForm').click(function(e) {
            e.preventDefault();
            var id = deleteID;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "productos/"+id,
                method: 'DELETE',
                success: function(result) {
                    $('#DeleteProductoModal').hide();
                }
            });
        });
    });
</script>
@endsection