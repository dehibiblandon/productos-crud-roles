@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<div class="container">
    <div class="row">

        <div class="col">
            <h1 class="alert-primary text-center"> Categorias </h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="/categorias" method="POST" class="form-inline">
                @csrf
                <div class="form-group mb-2">
                    <label for="staticEmail2" class="">Categoria</label>                    
                </div>
                <div class="form-group mx-sm-3 mb-2">                    
                <input type="text" class="form-control" id="nombre_categoria" name="nombre_categoria"
                        value="{{ old('nombre_categoria') }}">
                    @if ($errors->has('nombre_categoria'))
                    <small class="form-text text-danger">{{ $errors->first('nombre_categoria') }}</small>
                    @endif
                </div>
                <div class="form-group mb-2">
                <button type="submit" class="btn btn-primary mb-2">Guardar</button>
                
                <a class="" href="/categorias"> <button class="btn btn-success mb-2" type="button">Volver </button> </a>

                </div>
            </form>
        </div>
    </div>

    @endsection