@extends('layouts.app')
@section('content')

<div class="container">
      <div class="row">
        <div class="col">
            <h1 class="alert-primary text-center"> Categorias </h1>
        </div>
       
      </div>
      <div class="row">
        <h5 style="color: #f08102; text-align: center"><b>Lista de categorias </b></h5>
        
        <table class="table table-striped">

            <tr>
          
              <th>ID</th>
          
              <th>Nombre Categoría</th>
          
            </tr>
            @foreach ($categorias as $categoria)
            <tr>
          
            <td>{{$categoria->id}}</td>
          
            <td>{{$categoria->nombre_categoria}}</td>

            <td> 
              <a href="/categorias/{{$categoria->id}}/edit" class="btn btn-success btn-block">Edit</a>
            </td>
          <td>
              <form action="/categorias/{{$categoria->id}}" method="POST">
                @csrf
                @method('DELETE')<!--El boton submit ejecuta la acción del formulario-->
                    <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Está seguro que desea eliminar {{$categoria->nombre_categoria}} ')">Delete</button>
                </form>
            </td>
        </tr>
            @endforeach            
          
          </table>
          <div class="container">
          <div class="row">
            <a href="/categorias/create" class="btn btn-primary" style="float: right;">Agregar categoria</a>
          </div>
          </div>
  
        </div>  
        <hr>
      </div>
  
     
                
  

  </div>
</div><!--end cotainer-->

@endsection
