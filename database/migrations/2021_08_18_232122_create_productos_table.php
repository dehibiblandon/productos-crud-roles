<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_producto');
            $table->string('cantidad_producto');
            $table->integer('precio_producto_unidad');            
            $table->unsignedBigInteger('categoria_id');
            $table->unsignedBigInteger('estado_id');
//Creación de los fk. Por ORM en modelos se realiza relacion
            $table->foreign('estado_id')
                ->references('id')
                ->on('estado_productos')
                ->onDelete('cascade');
            $table->foreign('categoria_id')
                ->references('id')
                ->on('categoria')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
