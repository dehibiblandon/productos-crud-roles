<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();       

    	foreach (range(1,200) as $index) {
            DB::table('productos')->insert([
                'nombre_producto' => $faker->sentence(4),
                'cantidad_producto' => rand(5, 20),
                'precio_producto_unidad' => rand(1000,10000),
                'categoria_id' => rand(1, 7),
                'estado_id' => rand(1,2),
                'created_at' => $faker->date($format = 'Y-m-d', $max = 'now')
            ]);
        }
    }
}
