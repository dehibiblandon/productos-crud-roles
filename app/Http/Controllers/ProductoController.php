<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('producto.index');
    }

    public function getProductos(Request $request, Producto $producto)
    {
        $data = $producto->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {
                return '<button type="button" class="btn btn-success btn-sm" id="getEditProductoData" data-id="'.$data->id.'">Editar</button>
                    <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteProductoModal" class="btn btn-danger btn-sm" id="getDeleteId">Eliminar</button>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Producto $producto)
    {
        $validator = \Validator::make($request->all(), [
            'nombre_producto' => 'required',
            'cantidad_producto' => 'required',
            'precio_producto_unidad' => 'required',
            'categoria_id' => 'required',
            'estado_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $producto->storeData($request->all());

        return response()->json(['success'=>'Un nuevo producto se ha creado']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = new Producto;
        $data = $producto->findData($id);
      if($data->id != null){

      

        $html = '<div class="form-group">
                    <label for="nombre_producto">Nombre producto:</label>
                    <input type="text" class="form-control" name="nombre_producto" id="editnombre_producto" value="'.$data->nombre_producto.'">
                </div>
                <div class="form-group">
                <label for="cantidad_producto">Inventario:</label>
                <input type="text" class="form-control" name="cantidad_producto" id="editcantidad_producto" value="'.$data->cantidad_producto.'">
                </div>
                <div class="form-group">
                    <label for="precio_producto_unidad">Valor:</label>
                    <input type="text" class="form-control" name="precio_producto_unidad" id="editprecio_producto_unidad" value="'.$data->precio_producto_unidad.'">
                </div>
                <div class="form-group">
                <label for="categoria_id">Categoria:</label>
                <input type="text" class="form-control" name="categoria_id" id="editcategoria_id" value="'.$data->categoria_id.'">
            </div>
            <div class="form-group">
            <label for="estado_id">Valor:</label>
            <input type="text" class="form-control" name="estado_id" id="editestado_id" value="'.$data->estado_id.'">
        </div>';

        return response()->json(['html'=>$html]);
        }else{
            $err="no existe";
            return response()->json(['html'=>$err]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'nombre_producto' => 'required',
            'cantidad_producto' => 'required',
            'precio_producto_unidad' => 'required',
            'categoria_id' => 'required',
            'estado_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $producto = new Producto;
        $producto->updateData($id, $request->all());

        return response()->json(['success'=>'El producto se ha actualizado']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = new Producto;
        $producto->deleteData($id);

        return response()->json(['success'=>'El producto se ha eliminado']);
    }
}
