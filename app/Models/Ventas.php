<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    use HasFactory;

        //un usuario tiene muchos ventas
        public function user()
        {
            return $this->belongsTo(User::class);
        }
        //relacón muchos a muchos ventas y productos
        public function productos() {
            return $this->belongsToMany(Producto::class);
        }
}
