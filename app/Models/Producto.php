<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{   use HasFactory;
    protected $table = 'productos';
    protected $fillable = ['nombre_producto', 'cantidad_producto', 'precio_producto_unidad', 'categoria_id', 'estado_id'];
    
    //Creamos la función para acceder a la categoría propietaria del producto
    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }
//un estado producto tiene muchos productos
    public function estadoProducto()
    {
        return $this->belongsTo(EstadoProducto::class);
    }
   
        //relacón muchos a muchos ventas y productos
        public function ventas() {
            return $this->belongsToMany(Ventas::class);
        }
        public function getData()
        {
            return static::orderBy('created_at','desc')->get();
        }
    
        public function storeData($input)
        {
            return static::create($input);
        }
    
        public function findData($id)
        {
            return static::find($id);
        }
    
        public function updateData($id, $input)
        {
            return static::find($id)->update($input);
        }
    
        public function deleteData($id)
        {
            return static::find($id)->delete();
        }    
}
