<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstadoProducto extends Model
{
    use HasFactory;
        protected $table = 'estado_productos';
        protected $fillable = ['nombre_estado_producto'];
    
    //relaaciones, una categoria tiene muchos productos
    public function productos()
    {
        return $this->hasMany(Producto::class);
    }
}
