<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    use HasFactory;
    protected $table = 'tipo_usuarios';
    protected $fillable = ['nombre_rol'];

//relaaciones, una categoria tiene muchos productos
public function usuarios()
{
    return $this->hasMany(User::class);
}
}
