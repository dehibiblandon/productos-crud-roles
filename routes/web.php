<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\VentasController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LandingController::class, 'index']);

//Auth::routes(['register'=>false]);// No permita registros desde la aplicación
Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Route::resource('categorias', CategoriaController::class)->middleware('auth');

// Resource Route for producto.
Route::resource('productos', ProductoController::class)->middleware('auth');
// Route for get articles for yajra post request.
Route::get('get-productos', [ProductoController::class, 'getProductos'])->name('get-productos')->middleware('auth');
Route::resource('ventas', VentasController::class)->middleware('auth');

